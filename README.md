# OpenML dataset: Bike-Sharing-Washington-DC

https://www.openml.org/d/43486

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Climate change is forcing cities to re-imaging their transportation infrastructure. Shared mobility concepts, such as car sharing, bike sharing or scooter sharing become more and more popular. And if they are implemented well, they can actually contribute to mitigating climate change. Bike sharing in particular is interesting because no electricity of gasoline is necessary (unless e-bikes are used) for this mode of transportation. However, there are inherent problems to this type of shared mobility:

varying demand at bike sharing stations needs to be balanced to avoid oversupply or shortages
heavily used bikes break down more often

Forecasting the future demand can help address those issues. Moreover, demand forecasts can help operators decide whether to expand the business, determine adequate prices and generate additional income through advertisements at particularly busy stations.
But that's not all. Another challenge is redistributing bikes between stations and determining the optimal routes. And determining the location of new stations is also an area of interest for operators.
Content
This dataset can be used to forecast demand to avoid oversupply and shortages. It spans from January 1, 2011, until December 31, 2018. Determining new station locations, analyzing movement patterns or planning routes will only be possible with additional data.

date - date with the format yyyy-mm-dd
temp_avg - average daily temperature in degree Celsius
temp_min - minimum daily temperature in degree Celsius
temp_max - maximum daily temperature in degree Celsius
temp_observ - temperature at the time of observation in degree Celsius
precip - amount of precipitation in mm
wind - wind speed in meters per second
wt_fog - weather type fog, ice fog, or freezing fog (may include heavy fog)    
wtheavyfog - weather type heavy fog or heaving freezing fog (not always distinguished from fog)    
wt_thunder - weather type thunder
wt_sleet - weather type ice pellets, sleet, snow pellets, or small hail
wt_hail - weather type hail (may include small hail)
wt_glaze - weather type glaze or rime
wt_haze - weather type smoke or haze    
wtdriftsnow - weather type blowing or drifting snow
wthighwind - weather type high or damaging winds
wt_mist - weather type mist
wt_drizzle - weather type drizzle    
wt_rain - weather type rain (may include freezing rain, drizzle, and freezing drizzle)
wtfreezerain - weather type freezing rain    
wt_snow - weather type snow, snow pellets, snow grains, or ice crystals    
wtgroundfog - weather type ground fog
wticefog -    weather type ice fog or freezing fog
wtfreezedrizzle - weather type freezing drizzle
wt_unknown - weather type unknown source of precipitation
casual - number of unregistered customers
registered - number of registered customers
total_cust - sum of registered and casual customers
holiday - indicates whether the day is a holiday or not

Acknowledgements
The data I used to create this dataset was taken from:

Capital Bikeshare for the bike sharing demand,
NOAA's National Climatic Data Center for weather data,
DC Department of Human Resources for data on public holidays.

Inspiration
Think about the following questions/topics and add more data to this dataset to improve your results:

What will tomorrow's, next week's or next month's bike demand? Use time series analysis to determine this.
Use anomaly detection to identify seasonality and trend in daily customers data.
Which features are particularly important for the forecast of the bike demand?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43486) of an [OpenML dataset](https://www.openml.org/d/43486). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43486/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43486/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43486/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

